Extended avatar template for Matrix.

# Usage.
- Install the [Atkinson Hyperlegible](https://brailleinstitute.org/freefont) font.
- Open the [template](https://codeberg.org/cf/pfpinator/raw/branch/main/pfptemplate.svg) in [Inkscape](https://inkscape.org).
- Put in the image of your choice and fill out the text, following the instructions inside the file.
- Export the image in `.png`, make sure to only export the canvas and not the selection of everything.

# Keep in mind.
- Changing your global display name and/or avatar will override all your local display names *and* avatars (even if you only changed one and not both).
- [Not all clients let you see the full avatar](https://codeberg.org/cf/pfpinator/issues/1).
- [Not all clients let you upload a non-rectangular image as avatar](https://codeberg.org/cf/pfpinator/issues/2).